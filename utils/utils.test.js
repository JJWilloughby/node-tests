const expect = require('expect');

const utils = require('./utils');

it('should add two numbers', () => {
	var res = utils.add(33, 11);
	expect(res).toBe(44).toBeA('number');
});

it('should square a number', () => {
	var res = utils.square(9);
	expect(res).toBe(81).toBeA('number');
});

it('should verify first and last names are set', () => {
	var res = utils.setName({ }, 'Justin Willoughby');
	expect(res).toBeA('object').toInclude({
		firstName: 'Justin',
		lastName: 'Willoughby'
	});
});