# node-tests
Example of node testing using Mocha and Expect

## Installation
- Clone the repo
- Navigate to directory
- `npm install`

## Usage
- Run tests using `npm test`
- Run tests and reload upon changes `npm run test-watch`

## Purpose
I used this project as an introduction to testing node applications
